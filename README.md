# Sogeti assessment assignment
Made by Jeroen Hendriks as part of the intake procedure.

Firstly I would like to say thank you to whoever is going to review this. It was a very fun project and I hope it may lead to many more fun projects with Sogeti!

## project info
This is a React project using typescript.
Based on the `create-react-app` package.

And for some reason I thought it would be great idea to experiment a little. So I installed 'reactstrap' and 'react-bootstrap' together and used them together.

It turns out that reactstrap is still somewhat experimental so I had to fall back on 'react-bootstrap' a few times. The reason why I wanted to use 'reactstrap' is because it compacts larger 'react-bootstrap' elements into single packages/elements which you can modify/manipulate through `props`.

## Sogecine
Sogecine is a mashup of the words Sogeti and Cinema.

# Installing
Manjaro (Arch Linux): `pacman -Ss npm` & `sudo pacman -S npm`

# App setup & run
- pull the repo
- `npm install`
- `npm start sogeti-cinema`

This will pull the public & src folders and the package files. Running `npm install` will install all necessary node-modules.

# commands
## Run app
`npm start sogeti-cinema`

## Stop app
`ctrl + c` in the running terminal will shut the app down.
