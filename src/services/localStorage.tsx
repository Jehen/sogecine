import {IState as Filters, mediaTypes} from '../components/header/header';

class LS {
    setFilters(filters: Filters["filters"]):void {
        localStorage.setItem("SogeCineFilters", JSON.stringify(filters));
    }

    /**
     * getFilters returns stored filter preferences, if none exist a default will be returned.
     * @returns Fil
     */
    getFilters(): Filters["filters"] {
        const res = localStorage.getItem("SogeCineFilters");
        if (!res) {
            return ({
                isOpen: false,
                releaseActive: false,
                releaseYear: new Date().getFullYear(),
                format: mediaTypes.all,
                fullPlot: true
            });
        }
        return JSON.parse(res);
    }

    setSearchTerm(term: string):void {
        localStorage.setItem("SogeCineTerm", term);
    }

    //search terms are for single time use only, so once it is retrieved it is also removed.
    getSearchTerm(): string | null{
        const res = localStorage.getItem("SogeCineTerm");
        localStorage.removeItem("SogeCineTerm");
        return res;
    }
}

export default new LS();