import HTTPCall, {Methods, IResponse} from './httpCall';
import config from '../../config.json';
import {mediaTypes} from '../../components/header/header';

class Media {
    search(query:string, page:number, callback:(res:IResponse)=>any, format:string|null =null, release:number|null =null){
        var url = config.api.baseAddress +"?"+ config.api.params.search + query;
        //ommit mediaType if set to all because there is no "all" paramater in the API and default will retrieve all.
        if(format && format !== mediaTypes.all) {url += "&" + config.api.params.mediaType + format}
        if(release)                             {url += "&" + config.api.params.releaseYear + release}
        url += "&" + config.api.params.page + page + "&" + config.api.key

        HTTPCall.call(url, {
            method: Methods.get
        }, callback);
    }

    searchTitle(title:string, callback:(res:IResponse)=>any, format:string|null =null, release:number|null =null, plot:string|null =null){
        var url = config.api.baseAddress +"?"+ config.api.params.titleSearch + title;
        if(format && format !== mediaTypes.all) {url += "&" + config.api.params.mediaType + format}
        if(release)                             {url += "&" + config.api.params.releaseYear + release}
        if(plot)                                {url += "&" + config.api.params.plotLength + plot}
        url += "&" + config.api.key

        HTTPCall.call(url, {
            method: Methods.get
        }, callback);
    }

    searchId(id:string, callback:(res:IResponse)=>any, format:string|null =null, release:number|null =null, plot:string|null =null){
        var url = config.api.baseAddress +"?"+ config.api.params.idSearch + id;
        if(format && format !== mediaTypes.all) {url += "&" + config.api.params.mediaType + format}
        if(release)                             {url += "&" + config.api.params.releaseYear + release}
        if(plot)                                {url += "&" + config.api.params.plotLength + plot}
        url += "&" + config.api.key

        HTTPCall.call(url, {
            method: Methods.get
        }, callback);
    }
}

export default new Media();