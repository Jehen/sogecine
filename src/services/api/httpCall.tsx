export interface IResponse{
    statusCode: number,
    statusText: string,
    content?: any
}

export enum Methods{
    get = "GET",
    post = "POST",
    patch = "PATCH",
    put = "PUT",
    delete = "DELETE"
}

export const ContentHeaders = {
    json: { "Content-Type": "application/json" },
    text: { "Content-Type": "text/plain" }
} as const


class HTTPCall {
    async call(url:string, options:any, callback:(res:IResponse)=>any, debug=false){
        if (debug === true) {console.log(options);}
        try{
            let resp = await fetch(url, options);
            
            let cont = await resp.json();

            const res:IResponse = {statusCode: resp.status, statusText: resp.statusText, content: cont}
            if(debug === true){
                console.log(resp);
                console.log(options);
            }
            callback(res);
        } catch(e) {
            const res:IResponse = {statusCode: 503, statusText: "Couldn't reach server", content: e}
            if(debug === true){
                console.log(e);
                console.log(options);
            }
            callback(res);
        }
    }
}

export default new HTTPCall();