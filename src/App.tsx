import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import config from './config.json';

import Main from './pages/main';
import Featured from './pages/featured';
import NotFound from './pages/notFound';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path={config.pages.main} exact={true} component={Main}/>
          <Route path={config.pages.featured} exact={true} component={Featured}/>

          <Route path={config.pages.notFound} exact={false} component={NotFound}/>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
