import React, {useState, useEffect} from "react";
import {useHistory} from 'react-router';
import './itemDisplay.css'
import config from '../../config.json';
import { IResponse } from "../../services/api/httpCall";
import Media from '../../services/api/media';
import {Alert, UncontrolledTooltip, Spinner, Table, Button} from 'reactstrap';
import LS from '../../services/localStorage';
import { plotLength } from "../header/header";

interface IProps{
    display: displayType,
    imdbId?: string | null,
    searchData?: searchData | null
}

interface IState{
    resp: IResponse | null,
    err: string | null,
    loading: boolean,
    data: searchData | null,
    longPlot: boolean | null
}

interface searchData {
    Title: string,
    Year: string,
    Rated: string,
    Released: string,
    Runtime: string,
    Genre: string,
    Director: string,
    Writer: string,
    Actors: string,
    Plot: string,
    PlotShort?: string,
    Language: string,
    Country: string,
    Awards: string,
    Poster: string,
    Ratings: [
        {
            Source: string,
            Value: string
        }
    ],
    Metascore: string,
    imdbRating: string,
    imdbVotes: string,
    imdbID: string,
    Type: string,
    DVD: string,
    BoxOffice: string,
    Production: string,
    Website: string,
    Response: string
}



export enum displayType {
    result = "result",
    mini = "mini",
    complete = "complete"
}



const ItemDisplay:React.FC<IProps> = ({display, imdbId, searchData}) => {
    const his = useHistory();
    const handleRedirect = () => {his.push(config.pages.featured)}

    //================================================================== state(s)
    const [resp, setResp] = useState<IState["resp"]>(null);
    const [err, setErr] = useState<IState["err"]>(null);
    const [data, setData] = useState<IState["data"]>(null);
    const [loading, setLoading] = useState<IState["loading"]>(false);
    const [longPlot, setLongPlot] = useState<IState["longPlot"]>(null);


    // ================================================================= "action listeners"
    useEffect(()=>{
        if(!imdbId) {return}

        setLoading(true);
        if(imdbId && !resp){
            if(LS.getFilters().fullPlot){
                Media.searchId(imdbId, setResp, null, null, plotLength.long);
            }else{
            Media.searchId(imdbId, setResp);
            }
        }

        if(!resp){ return }
        if(resp.statusCode !== 200) {
            setErr("An error occurred: " + resp.statusCode + " " + resp.statusText);
            setLoading(false);
            return;
        }
        if(resp.content.Response === "False"){
            setErr("An error occurred: " + resp.content.Error);
            setLoading(false);
            return;
        }

        setData(resp.content);
        setLoading(false);
    },[imdbId, resp]);

    useEffect(()=>{
        if(searchData && searchData !== data){
            setData(searchData);
        }
    },[searchData, data]);

    useEffect(()=>{
        if(!data){ return }

        if(data.Plot.length > 200 && !data.PlotShort){
            const plt = data.Plot.substring(0, 200) + "...";
            setData({...data, PlotShort: plt});
            setLongPlot(false);
        }
    },[data]);


    // ================================================================== render components
    const stringFixer = (input:any) => {
        if(typeof input !== "string"){ return }
        if(input === "totalSeasons"){ return "Seasons"}

        const res = input.split(", ");
        if(res.length < 2){ return input }

        return (
            <ul>
                {res.map(el=>(
                    <li>
                        {el}
                    </li>
                ))}
            </ul>
        );
    }

    const renderTitle = () => {
        if(!data){ return }
        return (
            <div>
                <h5 className="displayTitle left-text">{data.Title}</h5>
                <p className="displaySubtitle right-text">{data.Type}<br />{data.Year}</p>
                <img className="centre-text displayPoster" src={data.Poster} alt=""/>
            </div>
        );
    }

    const renderRatings = () => {
        if(!data){ return }
        return (
            <div>
                {(data.Ratings.length > 0)?
                    <div className="centre-text m-3">
                        {(data.Ratings.length > 1)? <p className="noGlow">ratings:</p> : <p className="noGlow">rating:</p>}
                        {data.Ratings.map(el=>{
                            //generate random ID string to attach to tooltips so they don't jump back and forth.
                            const idstr = Math.random().toString(16).substr(2, 16);
                            return (
                                <React.Fragment>
                                    <span id={el.Source.replaceAll(" ", "")+idstr} className="m-1 badge">{el.Value}</span>
                                    <UncontrolledTooltip placement="top" target={el.Source.replaceAll(" ", "")+idstr}>
                                        {el.Source}
                                    </UncontrolledTooltip>
                                </React.Fragment>
                            );
                        })}
                    </div>
                : <p className="noGlow">No ratings available</p>}
            </div>
        );
    }

    const renderTrays = () =>{
        if(!data){ return }
        const omitionList = ["Poster", "Title", "Year", "Plot", "Metascore", "imdbRating", "imdbVotes", "imdbID", "Type", "Response", "PlotShort"];
        var buffer:JSX.Element[] = [];

        const entries = Object.entries(data);

        entries.forEach((value)=>{
            if (typeof value[1] === "string" || value[1] instanceof String){
                if(value[1] !== "N/A" && !omitionList.includes(value[0])){
                buffer.push(<tr>
                    <td>{stringFixer(value[0])}</td>
                    <td>{stringFixer(value[1])}</td>
                </tr>);
                }
            }
        });
        return buffer;
    }
    
    const renderTable = () => {
        return (
            <React.Fragment>
                <tbody>
                    {renderTrays()}
                </tbody>
            </React.Fragment>
        );
    }

    const renderPlot = () => {
        if(!data){ return }

        if(longPlot === false){
            return (
                <div className="noGlow plot">
                    <h5 className="plotHeader">Plot</h5>
                    <p>{data.PlotShort}</p>
                    <Button onClick={()=>{setLongPlot(true)}}>MORE</Button>
                </div>
            );
        }

        return (
            <div className="noGlow plot">
                <h5 className="plotHeader">Plot</h5>
                <p>{data.Plot}</p>
                {(longPlot === true)? <Button onClick={()=>{setLongPlot(false)}}>LESS</Button> : null}
            </div>
        );
    }



    // =================================================== render display types
    const renderResult = () => {
        return (
            <React.Fragment>
                {renderRatings()}
                {renderPlot()}
                <Table className="left-text">
                    {renderTable()}
                </Table>
            </React.Fragment>
        );
    }

    const renderComplete = () => {
        return (
            <div className="centre-text displayItem">
                {renderTitle()}
                {renderRatings()}
                {renderPlot()}
                <Table className="left-text">
                    {renderTable()}
                </Table>
            </div>
        );
    }

    const renderMini = () => {
        return (
            <div className="centre-text displayItem">
                {renderTitle()}
                {renderRatings()}
                <Button onClick={handleRedirect}>MORE</Button>
            </div>
        );
    }

    
    return (
        <React.Fragment>
            {(err)? <Alert color="danger">{err}</Alert> : null}

            {(!err && loading)?
                <div className="centre-text"><Spinner color="warning" className="m-4"/></div>
            : null}

            {(!err && !loading && display === displayType.result)?
                renderResult() : null
            }
            {(!err && !loading && display === displayType.complete)?
                renderComplete() : null
            }
            {(!err && !loading && display === displayType.mini)?
                renderMini() : null
            }
        </React.Fragment>
    );
}

export default ItemDisplay;