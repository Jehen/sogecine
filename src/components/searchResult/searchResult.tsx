import React, {useState, useEffect} from "react";
import './searchResult.css'
import { Row, Collapse, Button } from 'reactstrap';
import ItemDisplay, {displayType} from '../itemDisplay/itemDisplay';

interface IProps{
    result:{
        Title: string,
        Year: string,
        imdbID: string,
        Type: string,
        Poster: string
    }
}

interface IState{
    isOpen: boolean,
    allData: boolean
}

const SearchResult:React.FC<IProps> = ({result}) => {
    //state(s)
    const [isOpen, setOpen] = useState<IState["isOpen"]>(false);
    const [allData, setData] = useState<IState["allData"]>(false);


    //"action listeners"
    useEffect(()=>{
        setOpen(false);
        setData(false);
    },[result]);


    //css class helper
    const getClass = () => {
        if(isOpen){return "resultItemHeader headerBorderOpen"}
        return "resultItemHeader";
    }

    //Not all data is for films is retrieved, only the search data.
    //To get other required data, like actors, ratings, etc. a direct search by title or ID must be made.
    //And for the sake of scalability this info is only requested once the "MORE INFO" button is pressed.
    //To load all the data anyway you could simply remove the ternary (!allData) statement and load the ItemDisplay element directly.
    return (
        <div className="resultItem">
            <div onClick={()=>{setOpen(!isOpen)}}  className={getClass()}>
                <Row>
                    <h5 className="resultItemHeaderText">{result.Title}</h5>
                </Row>
                <Row>
                    <p className="resultItemSubheader">{result.Type}<br/>{result.Year}</p>
                </Row>
            </div>
            <Collapse isOpen={isOpen}>
                {(result.Poster !== "N/A")?
                    <div className="centre-text"><img src={result.Poster} alt="" className="poster"/></div>
                    :
                    <p className="centre-text">No poster available</p>
                }
                <div className="centre-text">
                    {(!allData)?
                        <Button className="m-2" onClick={()=>{setData(true)}}>MORE INFO</Button>
                    :
                        <ItemDisplay imdbId={result.imdbID} display={displayType.result}/>
                    }
                </div>
            </Collapse>
        </div>
    );
}

export default SearchResult;