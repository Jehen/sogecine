import React from "react";
import './searchPagination.css';
import {Container, Row, Pagination, PaginationItem, PaginationLink} from 'reactstrap';
import SearchResult from '../searchResult/searchResult';

interface IProps {
    data: {
        Search: [
            {
                Title: string,
                Year: string,
                imdbID: string,
                Type: string,
                Poster: string
            }
        ],
        totalResults: number,
        Response: string
    }
    page: number,
    submitHandle: any
}

const SearchPagination: React.FC<IProps> = ({data, page, submitHandle}) => {

    const calcPage = () => {
        //10 results per page.
        const maxNr = Math.ceil(data.totalResults/10);
        const minNr = 1;

        if(isNaN(maxNr)){ return <p>No results found</p>}
        
        //Using the Badge import from 'react-bootstrap' or 'reactstrap' does not work so we'll use our own
        if(maxNr < 2){ return <p>results: <span className="badge">{data.totalResults}</span></p>; }

        if(maxNr < 3){
            return (
                <div>
                    <p>results: <span className="badge">{data.totalResults}</span></p>
                    <Pagination className="d-flex justify-content-center">
                        <PaginationItem onClick={()=>{submitHandle(1)}}>
                            <PaginationLink>{1}</PaginationLink>
                        </PaginationItem>
                        <PaginationItem onClick={()=>{submitHandle(2)}}>
                            <PaginationLink>{2}</PaginationLink>
                        </PaginationItem>
                    </Pagination>
                </div>
            );
        }

        const previous = (page-1<minNr)? minNr : page-1;
        const next = (page+1>maxNr)? maxNr : page+1;

        return (
            <div>
                <p>results: <span className="badge">{data.totalResults}</span></p>
                <Pagination className="d-flex justify-content-center">
                    <PaginationItem>
                        {(page === minNr)?
                            <PaginationLink first disabled/>
                        :
                            <PaginationLink first onClick={()=>submitHandle(minNr)}/>
                        }
                    </PaginationItem>
                    <PaginationItem>
                        {(page === minNr)?
                            <PaginationLink previous disabled/>
                        :
                            <PaginationLink previous onClick={()=>submitHandle(previous)}/>
                        }   
                    </PaginationItem>
                    <PaginationItem>
                        <PaginationLink>{page}</PaginationLink>
                    </PaginationItem>
                    <PaginationItem>
                        {(page === maxNr)?
                            <PaginationLink next disabled/>
                        :
                            <PaginationLink next onClick={()=>submitHandle(next)}/>
                        }
                    </PaginationItem>
                    <PaginationItem>
                        {(page === maxNr)?
                            <PaginationLink last disabled/>
                        :
                            <PaginationLink last onClick={()=>submitHandle(maxNr)}/>
                        }
                    </PaginationItem>
                </Pagination>
            </div>
        );
    }

    return(
        <Container>
            <Row className="centre-text">
                {calcPage()}
            </Row>
            <Row>
                {(data.totalResults)? 
                    data.Search.map(el=>{
                        return (
                            <SearchResult result={el}/>
                        );
                    })
                :null}
            </Row>
        </Container>
    );
}

export default SearchPagination;