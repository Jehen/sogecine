import React from "react";
import './searchFilter.css'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input } from 'reactstrap';
import { InputGroup } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { mediaTypes } from "../header/header";
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import {IState as Props} from '../header/header'; 


interface IProps{
    filters: Props["filters"]
    changeHandle: React.Dispatch<React.SetStateAction<Props["filters"]>>
}

const SearchFilter: React.FC<IProps> = ({filters, changeHandle}) => {

    const changeFilters = (e: React.ChangeEvent<HTMLSelectElement | HTMLInputElement>) => {
        var val:any = e.target.value;
        //For some reason checkboxes only return "on" when calling onChange
        if(e.target.name === "releaseActive")   {(filters.releaseActive === true)?  val = false : val = true}
        if(e.target.name === "fullPlot")        {(filters.fullPlot === true)?       val = false : val = true}

        changeHandle({
            ...filters,
            [e.target.name]: val
        });
    }

    const handleAccept = () => {
        var res = {...filters, isOpen: false};
        //1888 is the date of the oldest film (Roundhay Garden Scene) on the IMDB database
        if(filters.releaseYear < 1888){
            res = {...filters, isOpen: false, releaseYear: 1888}
        }
        if(filters.releaseYear > new Date().getFullYear()){
            res = {...filters, isOpen: false, releaseYear: new Date().getFullYear()}
        }
        changeHandle(res);
    }

    const handleReset = () => {
        changeHandle({
            isOpen: false,
            releaseActive: false,
            releaseYear: new Date().getFullYear(),
            format: mediaTypes.all,
            fullPlot: true
        });
    }

    return(
        <Modal isOpen={filters.isOpen}>
            <ModalHeader>
                Set Search Filter
            </ModalHeader>
            <ModalBody>
                Release year:
                <InputGroup>
                    <InputGroup.Checkbox
                        name="releaseActive"
                        onChange={changeFilters}
                        checked={filters.releaseActive}/>
                    <Input
                        name="releaseYear"
                        type="number"
                        onChange={changeFilters}
                        defaultValue={filters.releaseYear}
                        disabled={!filters.releaseActive}/>
                </InputGroup>
                <br />
                Media type:
                <select name="format" onChange={changeFilters} className="form-control" defaultValue={filters.format}>
                    <option value={mediaTypes.all}>{mediaTypes.all}</option>
                    <option value={mediaTypes.film}>{mediaTypes.film}</option>
                    <option value={mediaTypes.series}>{mediaTypes.series}</option>
                    <option value={mediaTypes.episode}>{mediaTypes.episode}</option>
                </select>
                <br />
                Display full plot:<br />
                <Input name="fullPlot" type="checkbox" onChange={changeFilters} checked={filters.fullPlot} />
            </ModalBody>
            <ModalFooter className="d-flex justify-content-between">
                <Button name="isOpen" color="secondary" onClick={handleReset}>RESET</Button>
                <Button name="isOpen" color="warning" onClick={handleAccept}>
                    <FontAwesomeIcon icon={faCheck}/>
                </Button>
            </ModalFooter>
        </Modal>
    );
}

export default SearchFilter;