import React, {useState, useEffect} from "react";
import {useHistory} from 'react-router';
import config from '../../config.json';
import './header.css';
import logo from '../../SC.png'; 
import { Navbar, Nav, InputGroup, Button, FormControl } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faFilter } from '@fortawesome/free-solid-svg-icons'
import SearchFilter from '../searchFilter/searchFilter';
import LS from '../../services/localStorage';


interface IProps {
    search: string | null,
    searchHandle: React.Dispatch<React.SetStateAction<string | null>> | null,
    submitRes: any
}

export interface IState {
    search: string | null
    filters: {
        isOpen: boolean,
        releaseActive: boolean,
        releaseYear: number,
        format: mediaTypes,
        fullPlot: boolean
    }
}



export enum mediaTypes{
    all = "all",
    film = "movie",
    series = "series",
    episode = "episode"
}

export enum plotLength{
    short = "short",
    long= "full"
}



const Header:React.FC<IProps> = (props) => {
    const his = useHistory();

    //redirects
    const handleBrandClick = () => {his.push(config.pages.main)}
    const handleFeaturedClick = () => {his.push(config.pages.featured)}


    //state(s)
    const [search, setSearch] = useState<IState["search"]>(props.search);
    const [filters, setFilters] = useState<IState["filters"]>(LS.getFilters());


    //handlers
    const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(e.target.value);
        if(props.searchHandle){props.searchHandle(e.target.value)}
    }

    const handleFilter = () => {setFilters({...filters, isOpen: !filters.isOpen})}
    
    const handleSubmit = () => {
        if (!search){return}

        //I want to display the search results on the main page.
        //So we check if we are on the main page and if not save the query and go to main.
        if (his.location.pathname !== config.pages.main){
            LS.setSearchTerm(search);
            his.push(config.pages.main);
            return;
        }

        if(props.submitRes){props.submitRes()}
    }

    //keyboard support so you can search by hitting enter as well as clicking the search button.
    const handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if(e.code === "Enter"){handleSubmit()}
    }


    //"action listeners"
    useEffect(()=>{
        LS.setFilters(filters);
    },[filters]);

    useEffect(()=>{
        //synchronise state and props if props are given
        if(props.search && props.search !== search){
            setSearch(props.search);
        }
    },[props.search, search]);    


    //renders
    return(
        <React.Fragment>
            <SearchFilter filters={filters} changeHandle={setFilters}/>
            <Navbar bg="dark" expand="lg" sticky="top">
                <Navbar.Brand onClick={handleBrandClick}>
                    <img id="header-logo" src={logo} alt="logo"/>
                    SOGECINE
                </Navbar.Brand>

                <Navbar.Toggle aria-controls="navbarScroll" className="headerMenuButton"/>

                <Navbar.Collapse id="navbarScroll">
                    <Nav>
                        <Nav.Link onClick={handleFeaturedClick}>Featured</Nav.Link>
                        <Nav.Link href="https://gitlab.com/Jehen/sogecine" target="_blank">GitLab</Nav.Link>
                        <InputGroup>
                            {(search)?
                                <FormControl className="header-search" placeholder="Search films and series..."
                                onChange={handleSearch} defaultValue={search} onKeyPress={handleKeyPress}
                            />:
                                <FormControl className="header-search" placeholder="Search films and series..."
                                onChange={handleSearch} onKeyPress={handleKeyPress}
                            />}
                            <Button variant="warning" onClick={handleSubmit}>
                                <FontAwesomeIcon icon={faSearch}/>
                            </Button>
                            <Button variant="secondary" id="headerFilterButton" onClick={handleFilter}>
                                <FontAwesomeIcon icon={faFilter}/>
                            </Button>
                        </InputGroup>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </React.Fragment>
    );
}

export default Header;
