import React from "react";
import Header from '../components/header/header';
import {Container, Row, Col} from 'reactstrap';
import ItemDisplay, {displayType} from '../components/itemDisplay/itemDisplay';

const Featured = () => {
    return (
        <React.Fragment>
            <Header search={null} searchHandle={null} submitRes={null}/>
            <Container>
                <Row>
                    <Col md={6}>
                        <div className="m-2 p-2 bg-black">
                            <ItemDisplay imdbId="tt0076759" display={displayType.complete}/>
                        </div>
                    </Col>
                    <Col md={6}>
                        <div className="m-2 p-2 bg-black">
                            <ItemDisplay imdbId="tt1049413" display={displayType.complete}/>
                        </div>
                    </Col>
                </Row>
            </Container>
        </React.Fragment>
    );
}

export default Featured;