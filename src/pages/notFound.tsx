import React from 'react';
import { useHistory } from 'react-router';
import { Container, Row, Col, Jumbotron, Button } from 'reactstrap';

const NotFound = () => {
    const his = useHistory();

    const handleOnClick = () => {
        his.push("/");
    }

    return (
        <React.Fragment>
            <Container>
                <Row>
                    <Col>
                        <Jumbotron className="m-2 p-3 centre-text centre-align bg-black max-width-700">
                            <h1>404 Page not found</h1>
                            <p className="lead">
                                It appears the page you are looking for does not exist,<br />
                                or at least not on this address.
                            </p>
                            <hr className="my-2"/>
                            <p>
                                Click on the button below to return to the main page.
                            </p>
                            <Button color="warning" onClick={handleOnClick}>MAIN PAGE</Button>
                        </Jumbotron>
                    </Col>
                </Row>
            </Container>
        </React.Fragment>
    );
}

export default NotFound;