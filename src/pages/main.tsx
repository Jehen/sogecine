import React, {useState, useEffect} from 'react';
import Header from '../components/header/header';
import { Container, Row, Col, Spinner, Alert } from 'reactstrap';
import LS from '../services/localStorage';
import Media from '../services/api/media';
import { IResponse } from '../services/api/httpCall';
import SearchPagination from '../components/searchPagination/searchPagination';
import ItemDisplay, {displayType} from '../components/itemDisplay/itemDisplay';


interface IState {
    search: string | null,
    response: IResponse | null,
    page: number,
    processing: boolean,
    err: string | null,
    data: any
}

const Main = () => {
    //state(s)
    const [search, setSearch] = useState<IState["search"]>(null);
    const [resp, setResp] = useState<IState["response"]>(null);
    const [page, setPage] = useState<IState["page"]>(1);
    const [process, setProcess] = useState<IState["processing"]>(false);
    const [err, setErr] = useState<IState["err"]>(null);
    const [data, setData] = useState<IState["data"]>(null);


    //handlers
    const handleSubmit = (pageNr:number, storedQuery?:string|null, byTitle?:boolean) => {
        var q = ""
        //replace any whitespaces with %20 just in case
        if (storedQuery){q = storedQuery.replaceAll(" ", "%20")}
        if (search)     {q = search.replaceAll(" ", "%20")}

        //if no query was created abort search request
        if (q === ""){return}

        const f = LS.getFilters();
        
        setProcess(true);
        if(!byTitle){
            Media.search(q, pageNr, setResp, f.format, ((f.releaseActive)? f.releaseYear : null) );
        }else{
            Media.searchTitle(q, setResp, f.format, ((f.releaseActive)? f.releaseYear : null) );
        }
        setPage(pageNr);
        setProcess(false);
    }



    //"action listeners"
    useEffect(()=>{
        const res = LS.getSearchTerm();
        if (res){
            setSearch(res);
            handleSubmit(1, res);
        }
    },[search]);

    useEffect(()=>{
        if(resp){
            if(resp.statusCode !== 200){
                setErr("An error occurred: " + resp.statusCode + " " + resp.statusText);
                setData(null);
                return;
            }

            //I noticed that searching for the film "Up", the API displayed a too many results error.
            //If you search by title it will be found, so if this error occurs from the normal search we try search by title.
            if(resp.content.Response === "False" && resp.content.Error === "Too many results."){
                handleSubmit(1, search, true);
                setData(null);
                return;
            }

            if(resp.content.Response === "False"){
                setErr("An error occurred: " + resp.content.Error);
                setData(null);
                return;
            }

            setErr(null);
            setData(resp.content);
        }
    },[resp, search]);



    //renderers
    const renderProcessing = () => {
        return (
            <div className="centre-text m-2">
                <Spinner color=""/>
                <p>
                    loading...
                </p>
            </div>
        );
    }

    return (
        <React.Fragment>
            <Header search={search} searchHandle={setSearch} submitRes={()=>{handleSubmit(1)}}/>
            <Container>
                <Row>
                    <Col md={4}>
                        <div className="m-2 p-2 bg-black">
                            <h4>Featured</h4>
                            <ItemDisplay imdbId="tt0076759" display={displayType.mini}/>
                            <ItemDisplay imdbId="tt1049413" display={displayType.mini}/>
                        </div>
                    </Col>
                    <Col md={8}>
                        <div className="m-2 p-2 bg-black">
                            <h4>Media</h4>
                            {(err)? <Alert className="m-2" color="danger">{err}</Alert> : null }

                            {(!process && !resp && !err)? <p className="m-2">When you search the results will be displayed here.</p> : null}
                            
                            {(data && "Search" in data)? <SearchPagination data={data} page={page} submitHandle={handleSubmit}/> : null }
                            
                            {(data && !("Search" in data))?
                                <React.Fragment>
                                    <Alert color="warning" className="m-2">It appears your query had too many results, so a single search by title was done instead.</Alert>
                                    <ItemDisplay searchData={data} display={displayType.complete} />
                                </React.Fragment>
                            : null}
                            
                            {(process)? renderProcessing() : null }
                        </div>
                    </Col>
                </Row>
            </Container>
        </React.Fragment>
    );
}

export default Main;